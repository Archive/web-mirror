<!--#set var="TITLE" value="The site configuration directory" -->
<!--#include virtual="/includes/header.shtml"-->

<pre>
$Id$
</pre>

<p>
To use this module, you must create a configuration directory for your site.
You can use the <tt>martin-private</tt> and <tt>martin-cip</tt> directories
as an example for this. It does not matter where you put that directory on
your server machine, but you need to make <file>site/local</file> (inside your
checked out copy of the <em>web-mirror</em> mode) a symbolic link to that
directory.

<h2><file>local-config.pl</file></h2>

<p>
This is a perl script which defines a <code>%CONFIG</code> hash containing
local configuration values.

<p>
Example (from <tt>martin-private</tt>):

<pre>
%CONFIG = ('TAR'		=> 'gtar',
	   'DOCROOT'		=> '/pub/www/gnome/web',
	   'TOPDIR'		=> '/home/martin/gnomecvs/web-mirror',
	   'news_fullitems'	=> 'news/fullitems',
	   'GNOME_DOCU_DATA'	=> '/home/martin/gnomecvs/gnome-docu/data',
	   'GNOME_DOCU_TARGET'	=> '$(DOCROOT)/gnome-docu');
</pre>

<p>
You can put the following configuration items in this hash:

<dl>
<dt><code>TAR</code>
<dd>Name of your <file>tar</file> program (you should only need to set this
on machines where the system's <tt>tar</tt> is not GNU tar).

<dt><code>DOCROOT</code>
<dd>Full path name to the <em>Document Root</em> of your web server (ie. if
you want to have your mirror at <tt>/~gnome/</tt> on your web server, make
this variable point to that directory (in your filesystem)).

<dt><code>TOPDIR</code>
<dd>Full path name to your checked out copy of the <em>web-mirror</em> module.

<dt><code>GNOME_DOCU_DATA</code>
<dd>Full path name to the data directory of your checked out copy of the
<em>gnome-docu</em> module. Normally this is the directory <file>data</file>
inside the <em>gnome-docu</em> module.

<dt><code>GNOME_DOCU_TARGET</code>
<dd>Full path name to the directory where the HTML files from the
<em>gnome-docu</em> should be put. This should be somewhere below
<code>DOCROOT</code> so people can access them through your web server.
You can use (in GNU make syntax) variable substitutions like
<tt>$(DOCROOT)/gnome-docu</tt> here.

<dt><code>news_fullitems</code> (experimental)
<dd>Path name relative to <code>DOCROOT</code> where the news full items
should be placed.
</dl>

<h2><file>local-entities.pl</file></h2>

<p>
This is a perl script which defines a <code>%ENTITIES</code> hash containing
local SGML entities. The <tt>Makefile</tt> automatically creates the
<file>config-entities.sgml</file> file using this script.

<pre>
%ENTITIES = ('DocRoot' => '/web',
	     'StyleSheet' => '&DocRoot/style/gnome-docu.css',
	     'webmaster-mail' => 'webmaster@gnome.org'
	    );
</pre>

<p>
The following entities are currently supported:

<dl>
<dt><code>DocRoot</code>
<dd>HTTP-Path to the <em>Document Root</em> of your GNOME mirror (if you
have your GNOME mirror at <tt>http://www.yoursite.com/~gnome/</tt>, put
<tt>/~gnome</tt> here). You do not need to append the trailing slash.

<dt><code>StyleSheet</code>
<dd>HTTP-Path to the stylesheet that should be used by your HTML pages. You
can use SGML entities here like <tt>&amp;DocRoot/style/gnome-docu.css</tt>
in the example above.

<dt><code>webmaster-mail</code>
<dd>EMail-Address of the webmaster of your site (ie. your email address).<br>
<em><font size="-1">
[FIXME: Should this always be webmaster@gnome.org or should mirror
administrators put their own email here ?]
</font></em>
</dl>

<h2><file>site-entities.sgml</file></h2>

<p>
You can add content to the generated <file>config-entities.sgml</file> by
putting it into this file.

<p>
Example:

<pre>
&lt;!ENTITY html-body-start SYSTEM "html-body-start.sgml">
&lt;!ENTITY html-body-end SYSTEM "html-body-end.sgml">
</pre>

<h2>Automatically generated files</h2>

<p>
The following files in this directory are automatically generated:

<blockquote><pre>
config-entities.sgml
Makefile.Common
</pre></blockquote>


<!--#include virtual="/includes/signature.shtml"-->
<!--#include virtual="/includes/footer.shtml"-->

