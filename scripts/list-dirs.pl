#!/usr/bin/perl -w

require '../site/local/local-config.pl';
use POSIX;

unless ($#ARGV == 0) {
  die "Usage: $0 directory\n";
}

$dir = $ARGV [0];

opendir DIR, $dir or
  die "opendir ($dir): $!";
while ($file = readdir DIR) {
  next if $file =~ /^\./ or $file =~ /CVS/ or ! -d $dir.'/'.$file;
  print $file."\n";
}

closedir DIR;
