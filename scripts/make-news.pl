#!/usr/bin/perl -w

require '../site/local/local-config.pl';
use POSIX;
use strict;

use vars qw[%CONFIG $DIR $NEWS_ROOT $DOCTYPE @months %months @news_items
	    $fullitemsdir $normalitemsdir $news_index];

unless ($#ARGV == 0) {
  die "Usage: $0 directory\n";
}

$DIR = $ARGV [0];

$NEWS_ROOT = $CONFIG{DOCROOT}.'/news/items';
$DOCTYPE = "-//GNOME Documentation//DTD DocBook HTML 1.0//EN";

@months = qw[January February March April May June July
	     August September October November December];

%months = qw[January 0 February 1 March 2 April 3 May 4 June 5 July 6 
	     August 7 September 8 October 9 November 10 December 11];

opendir DIR, qq[$NEWS_ROOT/$DIR] or die "opendir ($DIR): $!";
@news_items = sort grep { !/^\./ && !/CVS/ && /\.item$/ } readdir DIR;
closedir DIR;

$fullitemsdir = $CONFIG{DOCROOT}.'/'.$CONFIG{news_fullitems};
unless (-d $fullitemsdir) {
  mkdir ($fullitemsdir, 0775) or die "mkdir ($fullitemsdir): $!";
}
$fullitemsdir .= '/'.$DIR;
unless (-d $fullitemsdir) {
  mkdir ($fullitemsdir, 0775) or die "mkdir ($fullitemsdir): $!";
}

$normalitemsdir = $CONFIG{DOCROOT}.'/'.$CONFIG{news_normalitems};
unless (-d $normalitemsdir) {
  mkdir ($normalitemsdir, 0775) or die "mkdir ($normalitemsdir): $!";
}
$normalitemsdir .= '/'.$DIR;
unless (-d $normalitemsdir) {
  mkdir ($normalitemsdir, 0775) or die "mkdir ($normalitemsdir): $!";
}

$news_index = $normalitemsdir.'/index.sgml';
open NEWSINDEX, "> $news_index" or
  die "open ($news_index): $!";

print NEWSINDEX qq{<!doctype html public "$DOCTYPE" \[\n};
print NEWSINDEX qq{  <!entity index-title '$DIR'>\n};
print NEWSINDEX qq{\]>\n};
print NEWSINDEX qq[&news-index-start;\n];
print NEWSINDEX qq[&news-date-start;$DIR&news-date-end;\n];

my $last_date = '';

foreach my $newsitem (@news_items) {
  $newsitem =~ /^(\d{4})(\d{2})(\d{2})_(\d+)\.item$/ or next;
  my ($year,$month,$day,$number) = ($1,$2,$3,$4);
  
  my ($date) = sprintf '%d %s %d', $day, $months[$month-1], $year;
  
  my $itemfile = qq[$NEWS_ROOT/$DIR/$newsitem];
  open ITEM, $itemfile or do {
    warn "open ($itemfile): $!";
    next;
  };
  
  my $title = <ITEM>; chop $title;
  my $subtitle = <ITEM>; chop $subtitle;
  my $text = '';
  
  my $title_entity = $title;
  $title_entity =~ s/[\'\"]//g;
  $title_entity =~ s/<[^\>]*>//g;

  my $line;  
  while (defined ($line = <ITEM>)) {
    last if $line =~ /^%FULLVERSION/;
    $text .= $line;
  }
  
  if (defined $line and $line =~ /^%FULLVERSION/) {
    my $fulltext = join '', <ITEM>;
    
    open FULLITEM, "> $fullitemsdir/$newsitem" or
      die "open ($fullitemsdir/$newsitem): $!";
    print FULLITEM qq{<!doctype html public "$DOCTYPE" \[\n};
    print FULLITEM qq{  <!entity table-title '$title_entity'>\n};
    print FULLITEM qq{  <!entity news-title '$title_entity'>\n};
    print FULLITEM qq{  <!entity news-date '$date'>\n};
    print FULLITEM qq{\]>\n};
    print FULLITEM qq[&news-fullitem-start;\n];
    print FULLITEM qq[&news-date-start;$date&news-date-end;\n];
    print FULLITEM qq[&news-title-start;$title&news-title-end;\n];
    print FULLITEM qq[&news-item-start;$fulltext&news-item-end;\n];
    print FULLITEM qq[&news-fullitem-end;\n];
    close FULLITEM;
  }
  
  my $normalitemsdir = $CONFIG{DOCROOT}.'/'.$CONFIG{news_normalitems}.'/'.$DIR;
  system "mkdir -p $normalitemsdir";
  
  open NORMALITEM, "> $normalitemsdir/$newsitem" or
    die "open ($normalitemsdir/$newsitem): $!";
  print NORMALITEM qq{<!doctype html public "$DOCTYPE" \[\n};
  print NORMALITEM qq{  <!entity table-title '$title_entity'>\n};
  print NORMALITEM qq{  <!entity news-title '$title_entity'>\n};
  print NORMALITEM qq{  <!entity news-date '$date'>\n};
  print NORMALITEM qq{\]>\n};
  print NORMALITEM qq[&news-fullitem-start;\n];
  print NORMALITEM qq[&news-date-start;$date&news-date-end;\n];
  print NORMALITEM qq[&news-title-start;$title&news-title-end;\n];
  print NORMALITEM qq[&news-item-start;$text&news-item-end;\n];
  print NORMALITEM qq[&news-fullitem-end;\n];
  close NORMALITEM;

  if ($date eq $last_date) {
    print NEWSINDEX qq[<br>\n];
  } else {
    if (!($last_date eq '')) {
      print NEWSINDEX qq[\n</blockquote></dd>\n</dl>\n\n];
    }
    print NEWSINDEX qq[<dl>\n];
    print NEWSINDEX qq[<dt><strong>$date</strong></dt>\n];
    print NEWSINDEX qq[<dd><blockquote>\n];
    $last_date = $date;
  }
  my $link = $newsitem; $link =~ s/\.item$/.html/;
  print NEWSINDEX qq[<a href="$link">$title</a>];
  
  close ITEM;
}

print NEWSINDEX qq[\n</blockquote></dd>\n</dl>\n];
print NEWSINDEX qq[&news-index-end;\n];
close NEWSINDEX;

my $makefile_content = '';
my $makefile = qq[$CONFIG{TOPDIR}/scripts/Makefile.news-fullitems.in];
open MAKEFILE_IN, $makefile or die "open ($makefile): $!";

while (defined ($_ = <MAKEFILE_IN>)) {
  s/\@TOPDIR@/$CONFIG{TOPDIR}/g;
  $makefile_content .= $_;
}

close MAKEFILE_IN;

open MAKEFILE, "> $normalitemsdir/Makefile" or die
  open "open ($normalitemsdir/Makefile): $!";
print MAKEFILE $makefile_content;
close MAKEFILE;

open MAKEFILE, "> $fullitemsdir/Makefile" or die
  open "open ($fullitemsdir/Makefile): $!";
print MAKEFILE $makefile_content;
close MAKEFILE;
