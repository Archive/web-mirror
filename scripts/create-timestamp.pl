#!/usr/bin/perl -w

require '../site/local/local-config.pl';
use POSIX;
use strict;

unless ($#ARGV == 0) {
  die "Usage: $0 directory\n";
}

my $dir = $ARGV [0];
my @newest = (undef, 0);

my $file;
opendir DIR, $dir or
  die "opendir ($dir): $!";
while ($file = readdir DIR) {
  next if $file =~ /^\./ or $file =~ /CVS/;

  my $mtime;
  my $path = qq[$dir/$file];

  if (-d $path) {
    $mtime = 0; my $subfile;
    opendir SUBDIR, $path or die "opendir ($path): $!";
    while ($subfile = readdir SUBDIR) {
      next if $subfile =~ /^\./ or $subfile =~ /CVS/;
      my $subpath = qq[$path/$subfile];

      my @stat = stat $subpath or die "stat ($subpath): $!";

      if ($stat [9] > $mtime) {
	$mtime = $stat [9];
      }
    }
    closedir SUBDIR;
  } else {
    my @stat = stat $path or die "stat ($path): $!";
    $mtime = $stat [9];
  };

  if ($mtime > $newest [1]) {
    $newest [0] = $file;
    $newest [1] = $mtime;
  }
}

closedir DIR;

die "No files found" unless defined $newest [0];

open TIMESTAMP, "> $dir/.timestamp" or
  die "open (.timestamp): $!";
printf TIMESTAMP "%s - %d\n", $newest [0], $newest [1];
close TIMESTAMP;

utime $newest [1], $newest [1], $dir.'/.timestamp' or
  die "utime: $!";
