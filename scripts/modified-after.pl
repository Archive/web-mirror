#!/usr/bin/perl -w

require '../site/local/local-config.pl';
use POSIX;
use strict;

unless (($#ARGV == 1) || ($#ARGV == 0)) {
  die "Usage: $0 directory [timestamp-file]\n";
}

my $dir = $ARGV [0];
my $file = ($#ARGV == 1) ? $ARGV [1] : qq[$dir/.timestamp];

my $timestamp;

if (-e $file) {
  my @stat = stat $file or die "stat ($file): $!";
  $timestamp = $stat [9];
} else {
  $timestamp = 0;
}

opendir DIR, $dir or die "opendir ($dir): $!";
while ($file = readdir DIR) {
  next if $file =~ /^\./ or $file =~ /CVS/;

  my $mtime;
  my $path = qq[$dir/$file];

  if (-d $path) {
    $mtime = 0; my $subfile;
    opendir SUBDIR, $path or die "opendir ($path): $!";
    while ($subfile = readdir SUBDIR) {
      next if $subfile =~ /^\./ or $subfile =~ /CVS/;
      my $subpath = qq[$path/$subfile];

      my @stat = stat $subpath or die "stat ($subpath): $!";

      if ($stat [9] > $mtime) {
	$mtime = $stat [9];
      }
    }
    closedir SUBDIR;
  } else {
    my @stat = stat $path or die "stat ($path): $!";
    $mtime = $stat [9];
  };
  
  if ($mtime > $timestamp) {
    print $file."\n";
  }
}

closedir DIR;
