#!/usr/bin/perl

use strict;
use vars qw[@FTPMIRRORS @WEBMIRRORS];

require 'MIRRORLIST.pl';

sub create_mirrors {
    my @MIRRORS = @_;

    my %LOCATIONS;
    foreach (@MIRRORS) {
	my ($id, $name, $url, $location, $active) = @$_;
	next unless $active;

	$LOCATIONS{$location}++;
    }

    my $location;
    foreach $location (keys %LOCATIONS) {
	print qq[<h2>$location</h2>\n\n<blockquote>\n];

	my $first = 1;
	foreach (@MIRRORS) {
	    my ($id, $name, $url, $loc, $active) = @$_;
	    next unless $active;
	    next unless $location =~ $loc;

	    if ($first) {
		$first = 0;
	    } else {
		print qq[<br>\n];
	    }

	    print qq[<a href="$url">$url</a>];
	}

	print qq[\n</blockquote>\n\n];
    }
}

my $prefix;
if ($#ARGV == 0) {
    $prefix = $ARGV [0];
} else {
    $prefix = '.';
}

open FTPMIRRORS, "> $prefix/ftpmirrors.sgml" or
    die "open ($prefix/ftpmirrors.sgml): $!";
select FTPMIRRORS;

print qq{<!doctype html public "-//GNOME Documentation//DTD DocBook HTML 1.0//EN" [\n  <!entity document-title 'GNOME FTP Mirrors'>\n]>\n\n&document-start;\n\n};
print qq{<h1>GNOME FTP Mirrors</h1>\n\n};

create_mirrors @FTPMIRRORS;

print qq{&document-end;\n\n};
close FTPMIRRORS;

open WEBMIRRORS, "> $prefix/webmirrors.sgml" or
    die "open ($prefix/webmirrors.sgml): $!";
select WEBMIRRORS;

print qq{<!doctype html public "-//GNOME Documentation//DTD DocBook HTML 1.0//EN" [\n  <!entity document-title 'GNOME WEB Mirrors'>\n]>\n\n&document-start;\n\n};
print qq{<h1>GNOME WEB Mirrors</h1>\n\n};

create_mirrors @WEBMIRRORS;

print qq{&document-end;\n\n};
close WEBMIRRORS;
