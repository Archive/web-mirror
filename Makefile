SUBDIRS		= site/local gnome-docu

all: subdirs

subdirs:
	for subdir in $(SUBDIRS) ; do \
	  $(MAKE) -C $$subdir ; \
	done

