%CONFIG = ('TAR'		=> 'gtar',
	   'DOCROOT'		=> '/home/martin/public_html/web',
	   'TOPDIR'		=> '/home/martin/gnomecvs/web-mirror',
	   'news_fullitems'	=> 'news/fullitems',
	   'news_normalitems'	=> 'news/normalitems',
	   'SGMLNORM_FLAGS'	=> '-wno-valid',
	   'GNOME_DOCU_DATA'	=> '/home/martin/gnomecvs/gnome-docu/data',
	   'GNOME_DOCU_TARGET'	=> '$(DOCROOT)/gnome-docu');
