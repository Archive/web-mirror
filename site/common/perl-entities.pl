#!/usr/bin/perl -w

require 'local-entities.pl';

print qq[<!-- Perl entities section -->\n\n];

foreach (keys %ENTITIES) {
  print qq[<!ENTITY $_ '$ENTITIES{$_}'>\n];
}

print qq[\n<!-- Automatic entities section -->\n\n];

printf qq[<!ENTITY current-date '%s'>\n], scalar gmtime;

if ($#ARGV >= 0) {
  print qq[\n<!-- Manual entities section -->\n\n];

  print <>;
}

print "\n";
