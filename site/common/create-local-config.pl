#!/usr/bin/perl -w

require 'local-config.pl';

foreach (keys %CONFIG) {
  printf qq[%-20s = %s\n], $_, $CONFIG{$_};
}

if ($#ARGV >= 0) {
  print <>;
}

print qq[\ninclude \$(TOPDIR)/site/common/Makefile.Common\n];

print "\n";
