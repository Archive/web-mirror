%CONFIG = ('TAR'		=> 'gtar',
	   'DOCROOT'		=> '/usr/local/www/newsite.gnome.org',
	   'TOPDIR'		=> '/usr/local/www/gnomeweb/gnomecvs/web-mirror',
	   'news_fullitems'	=> 'news/fullitems',
	   'news_normalitems'	=> 'news/normalitems',
	   'GNOME_DOCU_DATA'	=> '/usr/local/www/gnomeweb/gnomecvs/gnome-docu/data',
	   'GNOME_DOCU_TARGET'	=> '/usr/local/www/gnomeweb/public_html/gnome-docu');
