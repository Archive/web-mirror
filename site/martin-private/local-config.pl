%CONFIG = ('TAR'		=> 'gtar',
	   'DOCROOT'		=> '/pub/www/gnome/web',
	   'TOPDIR'		=> '/home/martin/gnomecvs/web-mirror',
	   'news_fullitems'	=> 'news/fullitems',
	   'news_normalitems'	=> 'news/normalitems',
	   'GNOME_DOCU_DATA'	=> '/home/martin/gnomecvs/gnome-docu/data',
	   'GNOME_DOCU_TARGET'	=> '$(DOCROOT)/gnome-docu');
